package br.com.suasapp.jwtlib.utils;

import br.com.suasapp.jwtlib.properties.JwtProperties;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

import static br.com.suasapp.jwtlib.constants.SecurityConstants.TOKEN_PREFIX;


@RequiredArgsConstructor
@Component
public class JwtUtils {

    private final JwtProperties jwtProperties;

    public String createToken(String username, Long expirationTimeMilis) {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + expirationTimeMilis))
                .sign(Algorithm.HMAC512(jwtProperties.getSecret().getBytes()));
    }

    public String verifyToken(String token) throws JWTVerificationException {
        return JWT.require(Algorithm.HMAC512(jwtProperties.getSecret().getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getSubject();
    }

}
