package br.com.suasapp.jwtlib.constants;

public class SecurityConstants {

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTHORIZATION_HEADER_STRING = "Authorization";
    public static final String REFRESH_HEADER_STRING = "Refresh";

}
