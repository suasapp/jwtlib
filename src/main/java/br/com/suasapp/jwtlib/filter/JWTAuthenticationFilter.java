package br.com.suasapp.jwtlib.filter;

import br.com.suasapp.jwtlib.dto.AuthenticationDTO;
import br.com.suasapp.jwtlib.properties.JwtProperties;
import br.com.suasapp.jwtlib.utils.JwtUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static br.com.suasapp.jwtlib.constants.SecurityConstants.*;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private JwtUtils jwtUtils;

    private JwtProperties jwtProperties;

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JwtProperties jwtProperties) {
        this.authenticationManager = authenticationManager;
        this.jwtProperties = jwtProperties;
        this.jwtUtils = new JwtUtils(jwtProperties);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            AuthenticationDTO authDTO = new ObjectMapper().readValue(request.getInputStream(), AuthenticationDTO.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            authDTO.getUsername(), authDTO.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {

        String token = jwtUtils.createToken(((User) authResult.getPrincipal()).getUsername(),
                jwtProperties.getExpirationTime());
        String refreshToken = jwtUtils.createToken(((User) authResult.getPrincipal()).getUsername(),
                jwtProperties.getRefreshExpirationTime());
        Cookie session = new Cookie("token", token);
        Cookie refresh = new Cookie("refresh", refreshToken);
        session.setHttpOnly(true);
        response.addCookie(session);
        response.addCookie(refresh);
        response.addHeader(AUTHORIZATION_HEADER_STRING,
                TOKEN_PREFIX + token);
        response.addHeader(REFRESH_HEADER_STRING,
                TOKEN_PREFIX + refreshToken);
    }
}
