package br.com.suasapp.jwtlib.filter;

import br.com.suasapp.jwtlib.properties.JwtProperties;
import br.com.suasapp.jwtlib.utils.JwtUtils;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static br.com.suasapp.jwtlib.constants.SecurityConstants.AUTHORIZATION_HEADER_STRING;
import static br.com.suasapp.jwtlib.constants.SecurityConstants.REFRESH_HEADER_STRING;
import static br.com.suasapp.jwtlib.constants.SecurityConstants.TOKEN_PREFIX;


public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private JwtUtils jwtUtils;

    private JwtProperties jwtProperties;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, JwtProperties jwtProperties) {
        super(authenticationManager);
        this.jwtProperties = jwtProperties;
        jwtUtils = new JwtUtils(jwtProperties);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(AUTHORIZATION_HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req, res);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request,
                                                                  HttpServletResponse response) {
        String token = request.getHeader(AUTHORIZATION_HEADER_STRING);
        String user = null;
        if (token != null) {
            try {
                user = jwtUtils.verifyToken(token);
            } catch (JWTVerificationException e) {
                String refreshToken = request.getHeader(REFRESH_HEADER_STRING);
                if (refreshToken != null) {
                    user = jwtUtils.verifyToken(refreshToken);
                    response.setHeader(AUTHORIZATION_HEADER_STRING,TOKEN_PREFIX + jwtUtils.createToken(user,
                            jwtProperties.getExpirationTime()));
                    response.setHeader(REFRESH_HEADER_STRING, TOKEN_PREFIX + jwtUtils.createToken(user,
                            jwtProperties.getRefreshExpirationTime()));
                }
            }
            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }
            return null;
        }
        return null;
    }
}
